create table USUARIO(
    NO_CONTRATO varchar(10) PRIMARY KEY,
    USER varchar(16),
    PASS varchar(30),
    NOMBRE varchar(50),
    APP varchar(30),
    APM varchar(30),
    TELEFONO varchar(10),
    CORREO varchar(70),
    CALLE varchar(100),
    NO_EXT varchar(4),
    NO_INT varchar(4),
    COLONIA varchar(50),
    CP varchar(5),
    MUNICIPIO varchar(50),
    ESTADO varchar(50)
    );
    
