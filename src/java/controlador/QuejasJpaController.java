/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.exceptions.NonexistentEntityException;
import controlador.exceptions.RollbackFailureException;
import entidad.Quejas;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author RodoGA
 */
public class QuejasJpaController implements Serializable {

    public QuejasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityTransaction etx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Quejas quejas) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            
            em = getEntityManager();
            etx = em.getTransaction();
            etx.begin();
            em.persist(quejas);
            etx.commit();
        } catch (Exception ex) {
            try {
                etx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Quejas quejas) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            etx=em.getTransaction();
            etx.begin();
            
            quejas = em.merge(quejas);
            etx.commit();
        } catch (Exception ex) {
            try {
                etx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = quejas.getNoReporte();
                if (findQuejas(id) == null) {
                    throw new NonexistentEntityException("The quejas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            etx=em.getTransaction();
            etx.begin();
            
            Quejas quejas;
            try {
                quejas = em.getReference(Quejas.class, id);
                quejas.getNoReporte();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The quejas with id " + id + " no longer exists.", enfe);
            }
            em.remove(quejas);
            etx.commit();
        } catch (Exception ex) {
            try {
                etx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Quejas> findQuejasEntities() {
        return findQuejasEntities(true, -1, -1);
    }

    public List<Quejas> findQuejasEntities(int maxResults, int firstResult) {
        return findQuejasEntities(false, maxResults, firstResult);
    }

    private List<Quejas> findQuejasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Quejas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Quejas findQuejas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Quejas.class, id);
        } finally {
            em.close();
        }
    }

    public int getQuejasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Quejas> rt = cq.from(Quejas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
