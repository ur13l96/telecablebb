/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.exceptions.IllegalOrphanException;
import controlador.exceptions.NonexistentEntityException;
import controlador.exceptions.PreexistingEntityException;
import controlador.exceptions.RollbackFailureException;
import entidad.Servicios;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entidad.Usuario;
import java.util.ArrayList;
import java.util.Collection;
import entidad.SolicitudLlamada;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author RodoGA
 */
public class ServiciosJpaController implements Serializable {

    public ServiciosJpaController(EntityManagerFactory emf) {
        
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Servicios servicios) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (servicios.getUsuarioCollection() == null) {
            servicios.setUsuarioCollection(new ArrayList<Usuario>());
        }
        if (servicios.getSolicitudLlamadaCollection() == null) {
            servicios.setSolicitudLlamadaCollection(new ArrayList<SolicitudLlamada>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Usuario> attachedUsuarioCollection = new ArrayList<Usuario>();
            for (Usuario usuarioCollectionUsuarioToAttach : servicios.getUsuarioCollection()) {
                usuarioCollectionUsuarioToAttach = em.getReference(usuarioCollectionUsuarioToAttach.getClass(), usuarioCollectionUsuarioToAttach.getNoContrato());
                attachedUsuarioCollection.add(usuarioCollectionUsuarioToAttach);
            }
            servicios.setUsuarioCollection(attachedUsuarioCollection);
            Collection<SolicitudLlamada> attachedSolicitudLlamadaCollection = new ArrayList<SolicitudLlamada>();
            for (SolicitudLlamada solicitudLlamadaCollectionSolicitudLlamadaToAttach : servicios.getSolicitudLlamadaCollection()) {
                solicitudLlamadaCollectionSolicitudLlamadaToAttach = em.getReference(solicitudLlamadaCollectionSolicitudLlamadaToAttach.getClass(), solicitudLlamadaCollectionSolicitudLlamadaToAttach.getIdSolicitud());
                attachedSolicitudLlamadaCollection.add(solicitudLlamadaCollectionSolicitudLlamadaToAttach);
            }
            servicios.setSolicitudLlamadaCollection(attachedSolicitudLlamadaCollection);
            em.persist(servicios);
            for (Usuario usuarioCollectionUsuario : servicios.getUsuarioCollection()) {
                usuarioCollectionUsuario.getServiciosCollection().add(servicios);
                usuarioCollectionUsuario = em.merge(usuarioCollectionUsuario);
            }
            for (SolicitudLlamada solicitudLlamadaCollectionSolicitudLlamada : servicios.getSolicitudLlamadaCollection()) {
                Servicios oldIdServicioOfSolicitudLlamadaCollectionSolicitudLlamada = solicitudLlamadaCollectionSolicitudLlamada.getIdServicio();
                solicitudLlamadaCollectionSolicitudLlamada.setIdServicio(servicios);
                solicitudLlamadaCollectionSolicitudLlamada = em.merge(solicitudLlamadaCollectionSolicitudLlamada);
                if (oldIdServicioOfSolicitudLlamadaCollectionSolicitudLlamada != null) {
                    oldIdServicioOfSolicitudLlamadaCollectionSolicitudLlamada.getSolicitudLlamadaCollection().remove(solicitudLlamadaCollectionSolicitudLlamada);
                    oldIdServicioOfSolicitudLlamadaCollectionSolicitudLlamada = em.merge(oldIdServicioOfSolicitudLlamadaCollectionSolicitudLlamada);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findServicios(servicios.getIdServicio()) != null) {
                throw new PreexistingEntityException("Servicios " + servicios + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Servicios servicios) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Servicios persistentServicios = em.find(Servicios.class, servicios.getIdServicio());
            Collection<Usuario> usuarioCollectionOld = persistentServicios.getUsuarioCollection();
            Collection<Usuario> usuarioCollectionNew = servicios.getUsuarioCollection();
            Collection<SolicitudLlamada> solicitudLlamadaCollectionOld = persistentServicios.getSolicitudLlamadaCollection();
            Collection<SolicitudLlamada> solicitudLlamadaCollectionNew = servicios.getSolicitudLlamadaCollection();
            List<String> illegalOrphanMessages = null;
            for (SolicitudLlamada solicitudLlamadaCollectionOldSolicitudLlamada : solicitudLlamadaCollectionOld) {
                if (!solicitudLlamadaCollectionNew.contains(solicitudLlamadaCollectionOldSolicitudLlamada)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SolicitudLlamada " + solicitudLlamadaCollectionOldSolicitudLlamada + " since its idServicio field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Usuario> attachedUsuarioCollectionNew = new ArrayList<Usuario>();
            for (Usuario usuarioCollectionNewUsuarioToAttach : usuarioCollectionNew) {
                usuarioCollectionNewUsuarioToAttach = em.getReference(usuarioCollectionNewUsuarioToAttach.getClass(), usuarioCollectionNewUsuarioToAttach.getNoContrato());
                attachedUsuarioCollectionNew.add(usuarioCollectionNewUsuarioToAttach);
            }
            usuarioCollectionNew = attachedUsuarioCollectionNew;
            servicios.setUsuarioCollection(usuarioCollectionNew);
            Collection<SolicitudLlamada> attachedSolicitudLlamadaCollectionNew = new ArrayList<SolicitudLlamada>();
            for (SolicitudLlamada solicitudLlamadaCollectionNewSolicitudLlamadaToAttach : solicitudLlamadaCollectionNew) {
                solicitudLlamadaCollectionNewSolicitudLlamadaToAttach = em.getReference(solicitudLlamadaCollectionNewSolicitudLlamadaToAttach.getClass(), solicitudLlamadaCollectionNewSolicitudLlamadaToAttach.getIdSolicitud());
                attachedSolicitudLlamadaCollectionNew.add(solicitudLlamadaCollectionNewSolicitudLlamadaToAttach);
            }
            solicitudLlamadaCollectionNew = attachedSolicitudLlamadaCollectionNew;
            servicios.setSolicitudLlamadaCollection(solicitudLlamadaCollectionNew);
            servicios = em.merge(servicios);
            for (Usuario usuarioCollectionOldUsuario : usuarioCollectionOld) {
                if (!usuarioCollectionNew.contains(usuarioCollectionOldUsuario)) {
                    usuarioCollectionOldUsuario.getServiciosCollection().remove(servicios);
                    usuarioCollectionOldUsuario = em.merge(usuarioCollectionOldUsuario);
                }
            }
            for (Usuario usuarioCollectionNewUsuario : usuarioCollectionNew) {
                if (!usuarioCollectionOld.contains(usuarioCollectionNewUsuario)) {
                    usuarioCollectionNewUsuario.getServiciosCollection().add(servicios);
                    usuarioCollectionNewUsuario = em.merge(usuarioCollectionNewUsuario);
                }
            }
            for (SolicitudLlamada solicitudLlamadaCollectionNewSolicitudLlamada : solicitudLlamadaCollectionNew) {
                if (!solicitudLlamadaCollectionOld.contains(solicitudLlamadaCollectionNewSolicitudLlamada)) {
                    Servicios oldIdServicioOfSolicitudLlamadaCollectionNewSolicitudLlamada = solicitudLlamadaCollectionNewSolicitudLlamada.getIdServicio();
                    solicitudLlamadaCollectionNewSolicitudLlamada.setIdServicio(servicios);
                    solicitudLlamadaCollectionNewSolicitudLlamada = em.merge(solicitudLlamadaCollectionNewSolicitudLlamada);
                    if (oldIdServicioOfSolicitudLlamadaCollectionNewSolicitudLlamada != null && !oldIdServicioOfSolicitudLlamadaCollectionNewSolicitudLlamada.equals(servicios)) {
                        oldIdServicioOfSolicitudLlamadaCollectionNewSolicitudLlamada.getSolicitudLlamadaCollection().remove(solicitudLlamadaCollectionNewSolicitudLlamada);
                        oldIdServicioOfSolicitudLlamadaCollectionNewSolicitudLlamada = em.merge(oldIdServicioOfSolicitudLlamadaCollectionNewSolicitudLlamada);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = servicios.getIdServicio();
                if (findServicios(id) == null) {
                    throw new NonexistentEntityException("The servicios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Servicios servicios;
            try {
                servicios = em.getReference(Servicios.class, id);
                servicios.getIdServicio();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The servicios with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<SolicitudLlamada> solicitudLlamadaCollectionOrphanCheck = servicios.getSolicitudLlamadaCollection();
            for (SolicitudLlamada solicitudLlamadaCollectionOrphanCheckSolicitudLlamada : solicitudLlamadaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Servicios (" + servicios + ") cannot be destroyed since the SolicitudLlamada " + solicitudLlamadaCollectionOrphanCheckSolicitudLlamada + " in its solicitudLlamadaCollection field has a non-nullable idServicio field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Usuario> usuarioCollection = servicios.getUsuarioCollection();
            for (Usuario usuarioCollectionUsuario : usuarioCollection) {
                usuarioCollectionUsuario.getServiciosCollection().remove(servicios);
                usuarioCollectionUsuario = em.merge(usuarioCollectionUsuario);
            }
            em.remove(servicios);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Servicios> findServiciosEntities() {
        return findServiciosEntities(true, -1, -1);
    }

    public List<Servicios> findServiciosEntities(int maxResults, int firstResult) {
        return findServiciosEntities(false, maxResults, firstResult);
    }

    private List<Servicios> findServiciosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Servicios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Servicios findServicios(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Servicios.class, id);
        } finally {
            em.close();
        }
    }

    public int getServiciosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Servicios> rt = cq.from(Servicios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
