/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidad.Servicios;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author RodoGA
 */
@Stateless
public class ServiciosFacade {

        private EntityManagerFactory em = Persistence.createEntityManagerFactory("TeleCableGitPU");
        private ServiciosJpaController jpa = new ServiciosJpaController(em);

    public Servicios encontrar(Integer id){
        return jpa.findServicios(id);
    }
    
    
}
