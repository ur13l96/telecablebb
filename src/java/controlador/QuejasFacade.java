/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidad.Quejas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author RodoGA
 */
@Stateless
public class QuejasFacade{

    private EntityManagerFactory em = Persistence.createEntityManagerFactory("TeleCableGitPU");
    private QuejasJpaController jpa = new QuejasJpaController(em);

    public void insertaJPA(Quejas q) throws Exception{
        jpa.create(q);
    }
    
}
