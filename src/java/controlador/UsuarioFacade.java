/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.exceptions.RollbackFailureException;
import entidad.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author RodoGA
 */
@Stateless
public class UsuarioFacade{

    private EntityManagerFactory em = Persistence.createEntityManagerFactory("TeleCableGitPU");
    private UsuarioJpaController jpa = new UsuarioJpaController(em);
    
    public Usuario Busca (String id){
        Usuario u = jpa.findUsuario(id);
        return u;
    }
    public void EditaJPA(Usuario u) throws RollbackFailureException, Exception{
        jpa.edit(u);
    }
    public Usuario buscaUsuario(String user){
        return jpa.findByUser(user);
    }
}
