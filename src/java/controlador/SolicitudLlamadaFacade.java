/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidad.SolicitudLlamada;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author RodoGA
 */
@Stateless
public class SolicitudLlamadaFacade {

    
    private EntityManagerFactory em= Persistence.createEntityManagerFactory("TeleCableGitPU");
    private SolicitudLlamadaJpaController jpa = new SolicitudLlamadaJpaController (em); 
    

    public void insertaJPA(SolicitudLlamada s) throws Exception{
        jpa.create(s);
    }
    
}
