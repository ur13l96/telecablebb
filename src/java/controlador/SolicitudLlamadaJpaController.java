/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.exceptions.NonexistentEntityException;
import controlador.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entidad.Servicios;
import entidad.SolicitudLlamada;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.transaction.UserTransaction;

/**
 *
 * @author RodoGA
 */
public class SolicitudLlamadaJpaController implements Serializable {

    public SolicitudLlamadaJpaController(EntityManagerFactory emf) {
        
        this.emf = emf;
    }
    private EntityTransaction etx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SolicitudLlamada solicitudLlamada) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            etx=em.getTransaction();
            etx.begin();
            
            Servicios idServicio = solicitudLlamada.getIdServicio();
            if (idServicio != null) {
                idServicio = em.getReference(idServicio.getClass(), idServicio.getIdServicio());
                solicitudLlamada.setIdServicio(idServicio);
            }
            em.persist(solicitudLlamada);
            if (idServicio != null) {
                idServicio.getSolicitudLlamadaCollection().add(solicitudLlamada);
                idServicio = em.merge(idServicio);
            }
            etx.commit();
        } catch (Exception ex) {
            try {
                etx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SolicitudLlamada solicitudLlamada) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            etx=em.getTransaction();
            etx.begin();
            
            SolicitudLlamada persistentSolicitudLlamada = em.find(SolicitudLlamada.class, solicitudLlamada.getIdSolicitud());
            Servicios idServicioOld = persistentSolicitudLlamada.getIdServicio();
            Servicios idServicioNew = solicitudLlamada.getIdServicio();
            if (idServicioNew != null) {
                idServicioNew = em.getReference(idServicioNew.getClass(), idServicioNew.getIdServicio());
                solicitudLlamada.setIdServicio(idServicioNew);
            }
            solicitudLlamada = em.merge(solicitudLlamada);
            if (idServicioOld != null && !idServicioOld.equals(idServicioNew)) {
                idServicioOld.getSolicitudLlamadaCollection().remove(solicitudLlamada);
                idServicioOld = em.merge(idServicioOld);
            }
            if (idServicioNew != null && !idServicioNew.equals(idServicioOld)) {
                idServicioNew.getSolicitudLlamadaCollection().add(solicitudLlamada);
                idServicioNew = em.merge(idServicioNew);
            }
            etx.commit();
        } catch (Exception ex) {
            try {
                etx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = solicitudLlamada.getIdSolicitud();
                if (findSolicitudLlamada(id) == null) {
                    throw new NonexistentEntityException("The solicitudLlamada with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            etx=em.getTransaction();
            etx.begin();
            
            SolicitudLlamada solicitudLlamada;
            try {
                solicitudLlamada = em.getReference(SolicitudLlamada.class, id);
                solicitudLlamada.getIdSolicitud();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The solicitudLlamada with id " + id + " no longer exists.", enfe);
            }
            Servicios idServicio = solicitudLlamada.getIdServicio();
            if (idServicio != null) {
                idServicio.getSolicitudLlamadaCollection().remove(solicitudLlamada);
                idServicio = em.merge(idServicio);
            }
            em.remove(solicitudLlamada);
            etx.commit();
        } catch (Exception ex) {
            try {
                etx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SolicitudLlamada> findSolicitudLlamadaEntities() {
        return findSolicitudLlamadaEntities(true, -1, -1);
    }

    public List<SolicitudLlamada> findSolicitudLlamadaEntities(int maxResults, int firstResult) {
        return findSolicitudLlamadaEntities(false, maxResults, firstResult);
    }

    private List<SolicitudLlamada> findSolicitudLlamadaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SolicitudLlamada.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SolicitudLlamada findSolicitudLlamada(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SolicitudLlamada.class, id);
        } finally {
            em.close();
        }
    }

    public int getSolicitudLlamadaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SolicitudLlamada> rt = cq.from(SolicitudLlamada.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
