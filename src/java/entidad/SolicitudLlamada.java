/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author RodoGA
 */
@Entity
@Table(name = "solicitud_llamada")
@NamedQueries({
    @NamedQuery(name = "SolicitudLlamada.findAll", query = "SELECT s FROM SolicitudLlamada s")
    , @NamedQuery(name = "SolicitudLlamada.findByIdSolicitud", query = "SELECT s FROM SolicitudLlamada s WHERE s.idSolicitud = :idSolicitud")
    , @NamedQuery(name = "SolicitudLlamada.findByNombre", query = "SELECT s FROM SolicitudLlamada s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "SolicitudLlamada.findByApp", query = "SELECT s FROM SolicitudLlamada s WHERE s.app = :app")
    , @NamedQuery(name = "SolicitudLlamada.findByApm", query = "SELECT s FROM SolicitudLlamada s WHERE s.apm = :apm")
    , @NamedQuery(name = "SolicitudLlamada.findByTelefono", query = "SELECT s FROM SolicitudLlamada s WHERE s.telefono = :telefono")
    , @NamedQuery(name = "SolicitudLlamada.findByCorreo", query = "SELECT s FROM SolicitudLlamada s WHERE s.correo = :correo")
    , @NamedQuery(name = "SolicitudLlamada.findByCodigoPostal", query = "SELECT s FROM SolicitudLlamada s WHERE s.codigoPostal = :codigoPostal")})
public class SolicitudLlamada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SOLICITUD")
    private Integer idSolicitud;
    @Size(max = 25)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 25)
    @Column(name = "APP")
    private String app;
    @Size(max = 25)
    @Column(name = "APM")
    private String apm;
    @Column(name = "TELEFONO")
    private Integer telefono;
    @Size(max = 25)
    @Column(name = "CORREO")
    private String correo;
    @Column(name = "CODIGO_POSTAL")
    private Integer codigoPostal;
    @JoinColumn(name = "ID_SERVICIO", referencedColumnName = "id_servicio")
    @ManyToOne(optional = false)
    private Servicios idServicio;

    public SolicitudLlamada() {
    }

    public SolicitudLlamada(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Servicios getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Servicios idServicio) {
        this.idServicio = idServicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSolicitud != null ? idSolicitud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudLlamada)) {
            return false;
        }
        SolicitudLlamada other = (SolicitudLlamada) object;
        if ((this.idSolicitud == null && other.idSolicitud != null) || (this.idSolicitud != null && !this.idSolicitud.equals(other.idSolicitud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.SolicitudLlamada[ idSolicitud=" + idSolicitud + " ]";
    }
    
}
