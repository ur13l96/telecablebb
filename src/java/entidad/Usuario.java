/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author RodoGA
 */
@Entity
@Table(name = "usuario")
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByNoContrato", query = "SELECT u FROM Usuario u WHERE u.noContrato = :noContrato")
    , @NamedQuery(name = "Usuario.findByUsuario", query = "SELECT u FROM Usuario u WHERE u.usuario = :usuario")
    , @NamedQuery(name = "Usuario.findByPass", query = "SELECT u FROM Usuario u WHERE u.pass = :pass")
    , @NamedQuery(name = "Usuario.findByNombre", query = "SELECT u FROM Usuario u WHERE u.nombre = :nombre")
    , @NamedQuery(name = "Usuario.findByApp", query = "SELECT u FROM Usuario u WHERE u.app = :app")
    , @NamedQuery(name = "Usuario.findByApm", query = "SELECT u FROM Usuario u WHERE u.apm = :apm")
    , @NamedQuery(name = "Usuario.findByTelefono", query = "SELECT u FROM Usuario u WHERE u.telefono = :telefono")
    , @NamedQuery(name = "Usuario.findByCorreo", query = "SELECT u FROM Usuario u WHERE u.correo = :correo")
    , @NamedQuery(name = "Usuario.findByCalle", query = "SELECT u FROM Usuario u WHERE u.calle = :calle")
    , @NamedQuery(name = "Usuario.findByNoExt", query = "SELECT u FROM Usuario u WHERE u.noExt = :noExt")
    , @NamedQuery(name = "Usuario.findByNoInt", query = "SELECT u FROM Usuario u WHERE u.noInt = :noInt")
    , @NamedQuery(name = "Usuario.findByColonia", query = "SELECT u FROM Usuario u WHERE u.colonia = :colonia")
    , @NamedQuery(name = "Usuario.findByCp", query = "SELECT u FROM Usuario u WHERE u.cp = :cp")
    , @NamedQuery(name = "Usuario.findByMunicipio", query = "SELECT u FROM Usuario u WHERE u.municipio = :municipio")
    , @NamedQuery(name = "Usuario.findByEstado", query = "SELECT u FROM Usuario u WHERE u.estado = :estado")})
public class Usuario implements Serializable {

    @JoinTable(name = "servicio_usuario", joinColumns = {
        @JoinColumn(name = "NO_CONTRATO", referencedColumnName = "NO_CONTRATO")}, inverseJoinColumns = {
        @JoinColumn(name = "ID_SERVICIO", referencedColumnName = "id_servicio")})
    @ManyToMany
    private Collection<Servicios> serviciosCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "NO_CONTRATO")
    private String noContrato;
    @Size(max = 16)
    @Column(name = "USUARIO")
    private String usuario;
    @Size(max = 30)
    @Column(name = "PASS")
    private String pass;
    @Size(max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 30)
    @Column(name = "APP")
    private String app;
    @Size(max = 30)
    @Column(name = "APM")
    private String apm;
    @Size(max = 10)
    @Column(name = "TELEFONO")
    private String telefono;
    @Size(max = 70)
    @Column(name = "CORREO")
    private String correo;
    @Size(max = 100)
    @Column(name = "CALLE")
    private String calle;
    @Size(max = 4)
    @Column(name = "NO_EXT")
    private String noExt;
    @Size(max = 4)
    @Column(name = "NO_INT")
    private String noInt;
    @Size(max = 50)
    @Column(name = "COLONIA")
    private String colonia;
    @Size(max = 5)
    @Column(name = "CP")
    private String cp;
    @Size(max = 50)
    @Column(name = "MUNICIPIO")
    private String municipio;
    @Size(max = 50)
    @Column(name = "ESTADO")
    private String estado;

    public Usuario() {
    }

    public Usuario(String noContrato, String usuario, String pass, String nombre, String app, String apm, String telefono, String correo, String calle, String noExt, String noInt, String colonia, String cp, String municipio, String estado) {
        this.noContrato = noContrato;
        this.usuario = usuario;
        this.pass = pass;
        this.nombre = nombre;
        this.app = app;
        this.apm = apm;
        this.telefono = telefono;
        this.correo = correo;
        this.calle = calle;
        this.noExt = noExt;
        this.noInt = noInt;
        this.colonia = colonia;
        this.cp = cp;
        this.municipio = municipio;
        this.estado = estado;
    }
    

    
    

    public String getNoContrato() {
        return noContrato;
    }

    public void setNoContrato(String noContrato) {
        this.noContrato = noContrato;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNoExt() {
        return noExt;
    }

    public void setNoExt(String noExt) {
        this.noExt = noExt;
    }

    public String getNoInt() {
        return noInt;
    }

    public void setNoInt(String noInt) {
        this.noInt = noInt;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noContrato != null ? noContrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.noContrato == null && other.noContrato != null) || (this.noContrato != null && !this.noContrato.equals(other.noContrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Usuario[ noContrato=" + noContrato + " ]";
    }

    public Collection<Servicios> getServiciosCollection() {
        return serviciosCollection;
    }

    public void setServiciosCollection(Collection<Servicios> serviciosCollection) {
        this.serviciosCollection = serviciosCollection;
    }
    
}
