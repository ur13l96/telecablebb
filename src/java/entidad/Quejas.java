/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author RodoGA
 */
@Entity
@Table(name = "quejas")
@NamedQueries({
    @NamedQuery(name = "Quejas.findAll", query = "SELECT q FROM Quejas q")
    , @NamedQuery(name = "Quejas.findByNoReporte", query = "SELECT q FROM Quejas q WHERE q.noReporte = :noReporte")
    , @NamedQuery(name = "Quejas.findByDescripcion", query = "SELECT q FROM Quejas q WHERE q.descripcion = :descripcion")
    , @NamedQuery(name = "Quejas.findByFechaQueja", query = "SELECT q FROM Quejas q WHERE q.fechaQueja = :fechaQueja")
    , @NamedQuery(name = "Quejas.findByStatus", query = "SELECT q FROM Quejas q WHERE q.status = :status")})
public class Quejas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "NO_REPORTE")
    private Integer noReporte;
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "FECHA_QUEJA")
    @Temporal(TemporalType.DATE)
    private Date fechaQueja;
    @Size(max = 15)
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "NO_CONTRATO", referencedColumnName = "NO_CONTRATO")
    @ManyToOne(optional = false)
    private Usuario noContrato;

    public Quejas() {
    }

    public Quejas(Integer noReporte) {
        this.noReporte = noReporte;
    }

    public Integer getNoReporte() {
        return noReporte;
    }

    public void setNoReporte(Integer noReporte) {
        this.noReporte = noReporte;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaQueja() {
        return fechaQueja;
    }

    public void setFechaQueja(Date fechaQueja) {
        this.fechaQueja = fechaQueja;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Usuario getNoContrato() {
        return noContrato;
    }

    public void setNoContrato(Usuario noContrato) {
        this.noContrato = noContrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noReporte != null ? noReporte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Quejas)) {
            return false;
        }
        Quejas other = (Quejas) object;
        if ((this.noReporte == null && other.noReporte != null) || (this.noReporte != null && !this.noReporte.equals(other.noReporte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Quejas[ noReporte=" + noReporte + " ]";
    }
    
}
