/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.QuejasFacade;
import entidad.Quejas;
import entidad.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author RodoGA
 */
@Named(value = "beanQuejas")
@SessionScoped
public class beanQuejas implements Serializable {

    Integer noReporte;
    private String descripcion;
    private Date fechaQueja;
    private String status;
    private Usuario noContrato;
    Quejas q;
    QuejasFacade fa = new QuejasFacade();
    private FacesContext fc = FacesContext.getCurrentInstance();
    private ExternalContext ec = fc.getExternalContext();

    public void insertaQueja() throws Exception{
        q = new Quejas();
        q.setDescripcion(descripcion);
        q.setFechaQueja(new Date());
        q.setStatus("Sin atender");
        q.setNoContrato(noContrato);
        fa.insertaJPA(q);
        
    }
    public Integer getNoReporte() {
        return noReporte;
    }

    public void setNoReporte(Integer noReporte) {
        this.noReporte = noReporte;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaQueja() {
        return fechaQueja;
    }

    public void setFechaQueja(Date fechaQueja) {
        this.fechaQueja = fechaQueja;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Usuario getNoContrato() {
        return noContrato;
    }

    public void setNoContrato(Usuario noContrato) {
        this.noContrato = noContrato;
    }
    public beanQuejas() {
    }
    
}
