/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.UsuarioFacade;
import entidad.Servicios;
import entidad.Usuario;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author RodoGA
 */
@Named(value = "beanUsuario")
@RequestScoped
public class beanUsuario {

    String noContrato, usuario, pass, nombre, app, apm, telefono, correo, calle, no_ext, no_int, colonia, cp, municipio, estado;
    UsuarioFacade fa = new UsuarioFacade();
    String queja;
    beanQuejas bq = new beanQuejas();
    String Servicios;
    String fecha;
    double total;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    public beanQuejas getBq() {
        return bq;
    }
    public void insertaQ() throws Exception{
        bq.setNoContrato(fa.Busca(this.noContrato));
        bq.insertaQueja();
        fc.addMessage("",new FacesMessage("Listo, ha sido enviada"));
        this.bq.setDescripcion("");
    }
    public void setBq(beanQuejas bq) {
        this.bq = bq;
        
    }
    public String getQueja() {
        return queja;
    }

    public void setQueja(String queja) {
        this.queja = queja;
    }
    Usuario u;
    boolean loggeo=false;
    
    public boolean isLoggeo() {
        return loggeo;
    }

    public void setLoggeo(boolean loggeo) {
        this.loggeo = loggeo;
    }
    private FacesContext fc = FacesContext.getCurrentInstance();
    private ExternalContext ec = fc.getExternalContext();
    public beanUsuario() {
    }
    
    public void inicio() throws IOException{
        this.loggeo=true;
        this.nombre=this.u.getNombre();
        this.noContrato=u.getNoContrato();
        String param = "?nom="+nombre+"&u="+usuario+"&c="+noContrato+"&l="+loggeo+"&s="+this.Servicios;
        ec.redirect(ec.getRequestContextPath()+"/faces/InicioUsuario.xhtml"+param);
    }
    public void login () throws IOException{
        u = fa.buscaUsuario(usuario);
        Servicios = u.getServiciosCollection().size()+"";
        if (u!=null){
            if (pass.equals(u.getPass())){
                inicio();
            }
            else{
                
                fc.addMessage("", new FacesMessage("Contraseña incorrecta"));
            }
        }
        else{
            fc.addMessage("", new FacesMessage("No existe usuario"));
        } 
    }
    public void irLoggeado1() throws IOException{
        String param="";
        if (loggeo){
            
        param = "?nom="+nombre+"&u="+usuario+"&c="+noContrato+"&l="+loggeo;
        
        }
        ec.redirect(ec.getRequestContextPath()+"/faces/InicioUsuario.xhtml"+param);
        
    }
    public void irLoggeado2() throws IOException{
        String param="";
        if (loggeo){
            
        param = "?nom="+nombre+"&u="+usuario+"&c="+noContrato+"&l="+loggeo;
        
        }
        
        ec.redirect(ec.getRequestContextPath()+"/faces/Quejas.xhtml"+param);
    }
    public void registro () throws Exception{
        u = fa.Busca(this.noContrato);
        if (u!=null){
            if (u.getUsuario()==null){
                Usuario u2 = fa.buscaUsuario(usuario);
                if (u2==null){
                    u.setCorreo(correo);
                    u.setPass(pass);
                    u.setUsuario(usuario);
                    fa.EditaJPA(u);
                    inicio();
                    
                }
                else{
                    fc.addMessage("", new FacesMessage("Parece que el nombre de usuario ya existe, intenta con otro"));
                }
            }
            else{
                fc.addMessage("", new FacesMessage("Parece que ya estas registrado"));
            }
        }
        else{
            fc.addMessage("", new FacesMessage("Parece que aún no eres suscriptor"));
        }
    }
    public void verificaLoggeo() throws IOException{
        if (!loggeo){
            ec.redirect(ec.getRequestContextPath()+"/faces/login.xhtml");
            
        }
        else{
            u=fa.Busca(this.noContrato);
            Collection c=u.getServiciosCollection();
            Iterator<Servicios> it = c.iterator();
            this.Servicios="";
            this.total=0;
            while(it.hasNext())
            {
                Servicios s = it.next();
                
                this.Servicios=this.Servicios+s.getTipo()+"->"+s.getDescripcion()+"\n";
                total=total+s.getCostoMes();
            }
            this.fecha=obtieneFecha();
            
        }
    }
    
    public String obtieneFecha(){
        Calendar fecha = Calendar.getInstance();   
        String fc="";
        int dia = fecha.get(Calendar.DATE);
        int mes = fecha.get(Calendar.MONTH);
        int annio = fecha.get(Calendar.YEAR);
        if (dia > 10) {
            mes++;
            fc = "" + dia + "/" + mes + "/" + annio;
        }
        else{
            fc = "10/" + mes + "/" + annio;
        }
        return fc;
    }

    public String getServicios() {
        return Servicios;
    }

    public void setServicios(String Servicios) {
        this.Servicios = Servicios;
    }
    public void buscaUsuario(){
        fa.buscaUsuario(usuario);
    }
    public String getNoContrato() {
        return noContrato;
    }

    public void setNoContrato(String noContrato) {
        this.noContrato = noContrato;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNo_ext() {
        return no_ext;
    }

    public void setNo_ext(String no_ext) {
        this.no_ext = no_ext;
    }

    public String getNo_int() {
        return no_int;
    }

    public void setNo_int(String no_int) {
        this.no_int = no_int;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
