/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.ServiciosFacade;
import controlador.SolicitudLlamadaFacade;
import entidad.Servicios;
import entidad.SolicitudLlamada;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author RodoGA
 */
@Named(value = "beanSolicitudLlamada")
@SessionScoped
public class beanSolicitudLlamada implements Serializable {
    private FacesContext fc = FacesContext.getCurrentInstance();
    private ExternalContext ec = fc.getExternalContext();
    SolicitudLlamadaFacade fa = new SolicitudLlamadaFacade();
    private String nombre,app,apm,correo;
    private Integer telefono,codigoPostal;
    SolicitudLlamada sol;
    private Integer serv;
    ServiciosFacade faS = new ServiciosFacade();
    public void enviarS() throws Exception{
        sol=new SolicitudLlamada();
        sol.setNombre(nombre);
        sol.setApp(app);
        sol.setApm(apm);
        sol.setCodigoPostal(codigoPostal);
        sol.setCorreo(correo);
        sol.setTelefono(telefono);
        Servicios ss = faS.encontrar(serv);
        sol.setIdServicio(ss);
        fa.insertaJPA(sol);
        ec.redirect(ec.getRequestContextPath()+"/faces/index.xhtml");
    }
    public Integer getServ() {
        return serv;
    }

    public void setServ(Integer serv) {
        this.serv = serv;
    }

    public FacesContext getFc() {
        return fc;
    }

    public void setFc(FacesContext fc) {
        this.fc = fc;
    }

    public ExternalContext getEc() {
        return ec;
    }

    public void setEc(ExternalContext ec) {
        this.ec = ec;
    }

    public SolicitudLlamadaFacade getFa() {
        return fa;
    }

    public void setFa(SolicitudLlamadaFacade fa) {
        this.fa = fa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    public beanSolicitudLlamada() {
    }
    
}
